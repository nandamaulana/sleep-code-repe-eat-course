package com.example.sleepcoderepeatcourse

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.request.RequestOptions

class DetailActivity : AppCompatActivity() {
    lateinit var toolbar: androidx.appcompat.widget.Toolbar
    lateinit var hero_img: ImageView
    lateinit var hero_title: TextView
    lateinit var harga_course: TextView
    lateinit var rating_course: TextView
    lateinit var jumlah_materi: TextView
    lateinit var total_durasi_course: TextView
    lateinit var deskripsi: TextView
    lateinit var btn_tambah: Button
    lateinit var btn_join: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        supportActionBar?.title = ""

        val intentBefore = intent

        hero_img = findViewById(R.id.hero_img)
        hero_title = findViewById(R.id.hero_title)
        harga_course = findViewById(R.id.harga_course)
        rating_course = findViewById(R.id.rating_course)
        jumlah_materi = findViewById(R.id.jumlah_materi)
        total_durasi_course = findViewById(R.id.total_durasi_course)
        deskripsi = findViewById(R.id.deskripsi)

        val options: RequestOptions = RequestOptions().transform(CenterCrop())

        Glide.with(hero_img.context)
            .load(intentBefore.getIntExtra("foto_course",R.drawable.ic_launcher_background))
            .apply(options)
            .into(hero_img)
        hero_title.text = intentBefore.getStringExtra("nama_course")
        harga_course.text = intentBefore.getStringExtra("harga_course")
        rating_course.text = intentBefore.getStringExtra("rating_course")
        Log.e("test int",intentBefore.getIntExtra("jumlah_materi_course",0).toString())
        jumlah_materi.text = intentBefore.getIntExtra("jumlah_materi_course",0).toString()
        total_durasi_course.text = intentBefore.getIntExtra("total_durasi_course",0).toString()
        deskripsi.text = intentBefore.getStringExtra("deskripsi_course")

        btn_tambah = findViewById(R.id.btn_tambah)
        btn_join = findViewById(R.id.btn_join)

        btn_tambah.setOnClickListener { Toast.makeText(applicationContext,"Kelas Berhasil Ditambah",Toast.LENGTH_SHORT).show()  }
        btn_join.setOnClickListener { Toast.makeText(applicationContext,"Berhasil Join Kelas",Toast.LENGTH_SHORT).show() }
    }
}
