package com.example.sleepcoderepeatcourse

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sleepcoderepeatcourse.adapter.AdapterGridRecyclerView
import com.example.sleepcoderepeatcourse.adapter.AdapterListRecyclerView
import com.example.sleepcoderepeatcourse.interfaces.OnClickCallback
import com.example.sleepcoderepeatcourse.model.DataModelCourse
import com.example.sleepcoderepeatcourse.model.ModelCourse
import java.util.*
import kotlin.concurrent.schedule


class MainActivity : AppCompatActivity(), OnClickCallback,
    View.OnClickListener {
    lateinit var toolbar: Toolbar
    lateinit var filter_course: EditText
    lateinit var styleList: ImageButton
    lateinit var rv_course: RecyclerView
    private var list_course: ArrayList<ModelCourse> = arrayListOf()
    private var temp_filter_list_course: ArrayList<ModelCourse> = arrayListOf()
    private var isList: Boolean = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.setOnMenuItemClickListener {
            startActivity(Intent(this,AboutMe::class.java))
            true
        }
        filter_course = findViewById(R.id.filter_course);
        filter_course.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                Timer("Filtering", false).schedule(500) {
                    temp_filter_list_course = arrayListOf()
                    for (item in list_course) {
                        if ((item.nama_course + item.deskripsi_course).contains(
                                s.toString(),
                                true
                            )
                        ) {
                            temp_filter_list_course.add(item)
                        }
                    }
                    this@MainActivity.runOnUiThread {
                        showFilterList()
                    }
                    if (temp_filter_list_course.size == 0)
                        this@MainActivity.runOnUiThread {
                            Toast.makeText(this@MainActivity,"Filter Tidak Ditemukan, Munculkan Semua",Toast.LENGTH_SHORT).show()
                            showRecyclerList()
                        }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
        styleList = findViewById(R.id.styleList);
        styleList.setOnClickListener(this)
        rv_course = findViewById(R.id.rv_course);
        list_course.addAll(DataModelCourse.getData)
        showRecyclerList()
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private fun showRecyclerList() {
        rv_course.layoutManager = LinearLayoutManager(this)
        val listAdapter = AdapterListRecyclerView(list_course, this)
        rv_course.adapter = listAdapter
        listAdapter.notifyDataSetChanged()
    }
    private fun showRecyclerGrid() {
        rv_course.layoutManager = GridLayoutManager(this,2)
        val listAdapter = AdapterGridRecyclerView(list_course, this)
        rv_course.adapter = listAdapter
        listAdapter.notifyDataSetChanged()
    }

    private fun showFilterList() {
        if (isList){
            rv_course.layoutManager = LinearLayoutManager(this)
            val filterListAdapter = AdapterListRecyclerView(temp_filter_list_course, this)
            rv_course.adapter = filterListAdapter
            filterListAdapter.notifyDataSetChanged()
        }else {
            rv_course.layoutManager = GridLayoutManager(this,2)
            val filterListAdapter = AdapterGridRecyclerView(temp_filter_list_course, this)
            rv_course.adapter = filterListAdapter
            filterListAdapter.notifyDataSetChanged()

        }
    }


    override fun onCardClick(view: View, position: Int) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("nama_course", list_course[position].nama_course)
        intent.putExtra("harga_course", list_course[position].harga_course)
        intent.putExtra("rating_course", list_course[position].rating_course)
        intent.putExtra("jumlah_materi_course", list_course[position].jumlah_materi_course)
        intent.putExtra("total_durasi_course", list_course[position].total_durasi_course)
        intent.putExtra("deskripsi_course", list_course[position].deskripsi_course)
        intent.putExtra("foto_course", list_course[position].foto_course)

        val option = ActivityOptionsCompat.makeSceneTransitionAnimation(
            this,
            Pair.create(view.findViewById(R.id.image_course),"hero_img"),
            Pair.create(view.findViewById(R.id.nama_course),"hero_title")
        ).toBundle()

        startActivity(intent,option)
    }

    override fun onClick(v: View?) {
        filter_course.text.clear()
        if (v == styleList) {
            if (isList) {
                isList = !isList
                Log.e("islist", isList.toString())
                styleList.setImageResource(R.drawable.ic_grid_grey_24dp)
                showRecyclerGrid()
            } else {
                isList = !isList
                Log.e("islist", isList.toString())
                styleList.setImageResource(R.drawable.ic_view_list_grey_24dp)
                showRecyclerList()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.profile,menu)
        return true
    }

}
