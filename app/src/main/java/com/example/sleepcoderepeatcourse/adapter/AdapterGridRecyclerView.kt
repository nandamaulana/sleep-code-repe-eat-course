package com.example.sleepcoderepeatcourse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.sleepcoderepeatcourse.R
import com.example.sleepcoderepeatcourse.interfaces.OnClickCallback
import com.example.sleepcoderepeatcourse.model.ModelCourse

class AdapterGridRecyclerView(var list_course: ArrayList<ModelCourse>, var listener : OnClickCallback) : RecyclerView.Adapter<AdapterGridRecyclerView.MyListViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyListViewHolder {
        return MyListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_grid_course, parent, false))
    }

    inner class MyListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var card_list: RelativeLayout = itemView.findViewById(R.id.card_list)
        var nama_course: TextView = itemView.findViewById(R.id.nama_course)
        var jumlah_course: TextView = itemView.findViewById(R.id.jumlah_course)
        var rating_course: TextView = itemView.findViewById(R.id.rating_course)
        var image_course: ImageView = itemView.findViewById(R.id.image_course)
    }

    override fun onBindViewHolder(holder: MyListViewHolder, position: Int) {
        val dataCourse = list_course[position];
        holder.card_list.setOnClickListener { listener.onCardClick(holder.card_list,position) }
        holder.nama_course.text = dataCourse.nama_course
        holder.jumlah_course.text = dataCourse.jumlah_materi_course.toString() + " Course"
        holder.rating_course.text = dataCourse.rating_course
        val options:RequestOptions = RequestOptions().transform(CenterCrop(),RoundedCorners(16))
        Glide.with(holder.itemView.context)
            .load(dataCourse.foto_course)
            .apply(options)
            .into(holder.image_course)
        holder.image_course.clipToOutline = true

    }



    override fun getItemCount(): Int {
        return list_course.size;
    }

}