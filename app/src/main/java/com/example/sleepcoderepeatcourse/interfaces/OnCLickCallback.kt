package com.example.sleepcoderepeatcourse.interfaces

import android.view.View

interface OnClickCallback {
    fun onCardClick(view : View, position : Int)
}