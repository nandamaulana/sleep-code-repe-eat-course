package com.example.sleepcoderepeatcourse.model

import com.example.sleepcoderepeatcourse.R

object DataModelCourse {
    private val nama_course = arrayOf(
        "Full Stack Javascript Course",
        "Golang Course",
        "Google Cloud Course",
        "Machine Learning Course",
        "Postgree + Express + React + Node (PERN) Course",
        "Python Course",
        "React Native Course",
        "Software Tester Course",
        "UI & UX Course",
        "Wordpress Course"
    )
    private val harga_course = arrayOf(
        "Rp. 268,000",
        "Rp. 100,000",
        "Rp. 500,000",
        "Rp. 325,000",
        "Rp. 628,000",
        "Rp. 120,000",
        "Rp. 250,000",
        "Rp. 149,000",
        "Rp. 329,000",
        "Rp. 399,000"
    )
    private val rating_course = arrayOf(
        "4.9",
        "3.2",
        "4.1",
        "4.2",
        "4.8",
        "4.5",
        "5.0",
        "4.6",
        "3.0",
        "3.5"
    )
    private val jumlah_materi_course = intArrayOf(
        96,
        13,
        433,
        65,
        67,
        147,
        70,
        139,
        65,
        13
    )
    private val total_durasi_course = intArrayOf(
        24,
        2,
        32,
        26,
        6,
        18,
        14,
        10,
        16,
        4
    )
    private val deskripsi_course = arrayOf(
        "Learn full-stack web development using JavaScript (ReactJS, NodeJS, LoopbackJS, Redux and Material-UI)!",
        "Learn how to do all DBMS directly from golang to postgresql.",
        "110,000+ GCP Students, 500+ Questions - Associate Cloud Engineer, Cloud Architect, Cloud Developer, Cloud Data Engineer",
        "Introductory Machine Learning course covering theory, algorithms and applications.",
        "Learn how to setup a React Fullstack app on AWS with a PostgreSQL database",
        "Learn Python with projects covering game & web development, web scraping, MongoDB, Django, PyQt, and data visualization!",
        "Take your react js skills to next level by building Native Android and IOS Apps using React Native",
        "Everything you need to learn about software testing.",
        "Adobe XD: User interface design and user exprience design professionaly with abobe xd from zero to hero course 2020",
        "website designing using Adobe XD, SEO, WordPress (Beginner to Advanced)"
    )
    private val foto_course = intArrayOf(
        R.drawable.full_stack_javascript,
        R.drawable.golang,
        R.drawable.google_cloud,
        R.drawable.machine_learning,
        R.drawable.pern,
        R.drawable.python,
        R.drawable.react_native,
        R.drawable.software_tester,
        R.drawable.ui_ux,
        R.drawable.wordpress
    )

    val getData : ArrayList<ModelCourse>
    get() {
        val tempList = arrayListOf<ModelCourse>()
        for (position in nama_course.indices){
            tempList.add(ModelCourse(nama_course[position],
                    harga_course[position],
                    rating_course[position],
                    jumlah_materi_course[position],
                    total_durasi_course[position],
                    deskripsi_course[position],
                    foto_course[position]))
        }
        return tempList
    }
}