package com.example.sleepcoderepeatcourse.model

data class ModelCourse(
    var nama_course : String = "",
    var harga_course : String = "",
    var rating_course : String = "",
    var jumlah_materi_course : Int = 0,
    var total_durasi_course : Int = 0,
    var deskripsi_course : String = "",
    var foto_course  : Int = 0
)